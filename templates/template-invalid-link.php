<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-header.php'); ?>
    <body class="body-color">
        <div class="top-line">
            <div class="container">
                <div class="col-6-left">
                    <a href="/">
                        <div class="logo">
                            <div class="eq-wrapper">
                                <div class="equalizer"></div>
                            </div>
                        </div>
                        <h3>Soundcloudify</h3>
                    </a>
                </div>
                <div class="col-6-right">
                    <ul>
                        <li>About</li>
                        <li>Terms of use</li>
                        <li>Contacts</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="bg-image">
            <div class="container-mini padding-rule">
                <div class="error-wrapper">
                    <h3>You specified a wrong link</h3>
                    <p><?= $url; ?></p>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">
                <p>Soundcloudify.com - All rights reserved</p>
                <ul>
                    <li>About</li>
                    <li>Terms of use</li>
                    <li>Contacts</li>
                </ul>
            </div>
        </footer>
    </body>
</html>
