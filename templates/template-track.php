<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-header.php'); ?>
    <body class="body-color">
        <div class="top-line">
            <div class="container">
                <div class="col-6-left">
                    <a href="/">
                        <div class="logo">
                            <div class="eq-wrapper">
                                <div class="equalizer"></div>
                            </div>
                        </div>
                        <h3>Soundcloudify</h3>
                    </a>
                </div>
                <div class="col-6-right">
                    <ul>
                        <li>About</li>
                        <li>Terms of use</li>
                        <li>Contacts</li>
                    </ul>
                </div>
            </div>
        </div>
<?php
echo '
        <div class="bg-image">
            <div class="container-mini padding-rule">
                <div class="artist-wrapper">
                    <div class="artwork-common artwork-single">
                        '.$thumbnail_block.'
                        <form action="tw8cveGV25ar.php" method="post" class="single-form">
                            <span class="filesize">File size: '.$file_size.'</span>
                            <input type="hidden" name="final" value="'.$stream_url_real.'">
                            <input type="hidden" name="title" value="'.$file_name.'">
                            <input value="Download" type="submit">
                        </form>
                    </div>

                    <div class="artist-info">
                        <div class="by">'.$user.'</div>
                        <div class="name">'.$title.'</div>
                    </div>
                    
                    <div id="audioplayer">
                        <button id="pButton" class="play"></button>
                        <div id="timeline">
                            <div id="playhead"></div>
                        </div>
                        <audio id="music" preload="true">
                            <source src="'.$stream_url_real.'" type="audio/mpeg">
                        </audio>
                    </div>
                </div>
            </div>
        </div>
';
?>

        <footer>
            <div class="container">
                <p>Soundcloudify.com - All rights reserved</p>
                <ul>
                    <li>About</li>
                    <li>Terms of use</li>
                    <li>Contacts</li>
                </ul>
            </div>
        </footer>

        <script src="/scripts/main.js"></script>
    </body>
</html>
