<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-header.php'); ?>
    <body class="body-color">
        <div class="top-line">
            <div class="container">
                <div class="col-6-left">
                    <a href="/">
                        <div class="logo">
                            <div class="eq-wrapper">
                                <div class="equalizer"></div>
                            </div>
                        </div>
                        <h3>Soundcloudify</h3>
                    </a>
                </div>
                <div class="col-6-right">
                    <ul>
                        <li>About</li>
                        <li>Terms of use</li>
                        <li>Contacts</li>
                    </ul>
                </div>
            </div>
        </div>
<?php
$count = count($results);
echo '
        <div class="bg-image">
            <div class="container-mini padding-rule">
                <div class="artist-wrapper">
                    <div class="artist-info">
                        <div class="by">Search: '.$query.'</div>
				    </div>
';
if($count < 1) {
	echo '
                    <div class="error error-user">
                        <h3>Oops! Maybe playlist not contain songs or they are not available in your country.</h3>
                        <hr>
                    </div>';
}
else {
    echo '
                    <div class="playlist">';
	$i = 1;
	foreach ($results as $track) {
	    //echo $result->{'id'} . "<br>";
		if (IsNullOrEmpty($track->{'id'}))
			continue;
		$track_title = str_replace(' ', '&nbsp;', $track->{'title'});
		if (IsNullOrEmpty($track_title))
			$track_title = "Untitled" . $i;
		$soundcloud_url = $track->{'permalink_url'};
		$thumbnail = str_replace(array("mini", "tiny", "small", "badge", "t67x67", "large", "t300x300", "crop"), "crop", $track->{'artwork_url'});
		if (is_null($thumbnail) || strlen($thumbnail) == 0)
			$thumbnail = str_replace(array("mini", "tiny", "small", "badge", "t67x67", "large", "t300x300", "crop"), "crop", $track->{'user'}->{'avatar_url'});
		$thumbnail_track = (is_null($thumbnail) || strlen($thumbnail) == 0 || strpos($thumbnail, 'default_avatar') !== false) ? '<div class="cover cover-tune"></div>' : '<img src="'.$thumbnail.'">';
		echo '
                        <div class="track">
                            '.$thumbnail_track.'
                            <span class="num">'.$i.'</span>
                            <h4>'.$track->{'title'}.' ('.$track->{'user'}->{'username'}.')</h4>
                            <form action="pGz3ZbeCHw.php" method="post">
                                <input type="hidden" name="id" value="'.$track->{'id'}.'">
                                <input type="hidden" name="title" value="'.$track_title.'">
                                <input type="hidden" name="url" value="'.$soundcloud_url.'">
                                <input type="submit" class="btn" value="Download">
                            </form>
                        </div>';
		++$i;
	}
    echo '
                    </div>';
}
echo '
                </div>
            </div>
        </div>
        ';
?>

        <footer>
            <div class="container">
                <p>Soundcloudify.com - All rights reserved</p>
                <ul>
                    <li>About</li>
                    <li>Terms of use</li>
                    <li>Contacts</li>
                </ul>
            </div>
        </footer>
    </body>
</html>
