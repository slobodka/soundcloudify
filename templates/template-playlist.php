<?php include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-header.php'); ?>
    <body class="body-color">
        <div class="top-line">
            <div class="container">
                <div class="col-6-left">
                    <a href="/">
                        <div class="logo">
                            <div class="eq-wrapper">
                                <div class="equalizer"></div>
                            </div>
                        </div>
                        <h3>Soundcloudify</h3>
                    </a>
                </div>
                <div class="col-6-right">
                    <ul>
                        <li>About</li>
                        <li>Terms of use</li>
                        <li>Contacts</li>
                    </ul>
                </div>
            </div>
        </div>
<?php
echo '
        <div class="bg-image">
            <div class="container-mini padding-rule">
                <div class="artist-wrapper">
                    <div class="artwork-common artwork-playlist">
                        '.$thumbnail_block.'
                    </div>

                    <div class="artist-info">
                        <div class="by">'.$user.'</div>
                        <div class="name">'.$title.'</div>
                    </div>
                    
                    <div class="playlist">';
$i = 1;
foreach ($tracks as $track) {
    $track_title = str_replace(' ', '&nbsp;', $track->{'title'});
    //$track_thumbnail = str_replace(array("mini", "tiny", "small", "badge", "t67x67", "large", "t300x300", "crop"), "crop", $track->{'artwork_url'});
    //$thumbnailTrack = (is_null($track_thumbnail) || strlen($track_thumbnail) == 0) ? "<div class=\"cover\"></div>" : "<img src=\"{$track_thumbnail}\" alt=\"{$title}\">";
    echo '
                        <div class="track">
                            <span class="num">'.$i.'</span>
                            <h4>'.$track->title.'</h4>
                            <form action="pGz3ZbeCHw.php" method="post">
                                <input type="hidden" name="id" value="'.$track->id.'">
                                <input type="hidden" name="title" value="'.$track_title.'">
                                <input type="submit" value="Download">
                            </form>
                        </div>';
    ++$i;
}
echo '
                    </div>
                </div>
            </div>
        </div>
        ';
?>

        <footer>
            <div class="container">
                <p>Soundcloudify.com - All rights reserved</p>
                <ul>
                    <li>About</li>
                    <li>Terms of use</li>
                    <li>Contacts</li>
                </ul>
            </div>
        </footer>
    </body>
</html>
