<?php
require $_SERVER['DOCUMENT_ROOT'].'/process/global.php';

// #####################################################################################################################
// Empty request
if (empty($_REQUEST)) {
	header('Location: ../');
	exit;
}

// #####################################################################################################################
// Search
else if (isset($_POST['url']) && !IsUrl($_POST['url'])) {
	$query = preg_replace('(`|~|!|@|#|\$|%|\^|&|\*|\(|\)|-|_|=|\+|;|:|\'|"|<|>|,|\.|/|\\|\?|\|)', ' ', $_POST['url']);
	$query = preg_replace('!\s+!', '+', $query);
	$query = ltrim($query, '+');
	$query = rtrim($query, '+');
	$query = urlencode($query);
	$count = 50;
	$response = LoadData("http://api.soundcloud.com/tracks.json?q={$query}&limit={$count}&order=hotness&client_id={$api}");
	if (IsNullOrEmpty($response)) {
		header('refresh:5;url=../');
		//Logger(LogType::ERROR, ErrorCode::NO_RESPONSE, $link);
		include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-unavailable.php');
		exit;
	}
	$data = json_decode($response["content"]);
	if (IsNullOrEmpty($data)) {
		header('refresh:5;url=../');
		//Logger(LogType::ERROR, ErrorCode::NO_JSON, $link);
		include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-unavailable.php');
		exit;
	}
	if (array_key_exists('errors', $data)) {
		header('refresh:5;url=../');
		//Logger(LogType::ERROR, ErrorCode::ANOTHER_SC_LINK, $link);
		echo GetTemplate($_SERVER['DOCUMENT_ROOT'].'/templates/template-invalid-soundcloud.php', array('url' => $_POST['url']));
		exit;
	}
	$args = array(
		'results'           => $data,
		'query'             => $_POST['url']
	);
	echo GetTemplate($_SERVER['DOCUMENT_ROOT'].'/templates/template-search.php', $args);
	exit;
}

// #####################################################################################################################
// Invalid link
else if (isset($_POST['url']) && !IsUrlSoundcloud($_POST['url'])) {
	header('refresh:5;url=../');
	//Logger(LogType::ERROR, ErrorCode::INVALID_LINK, $_POST['url']);
	echo GetTemplate($_SERVER['DOCUMENT_ROOT'].'/templates/template-invalid-link.php', array('url' => $_POST['url']));
	exit;
}

// #####################################################################################################################
// Resolve
else if (isset($_POST['url']) && IsUrlSoundcloud($_POST['url'])) {
	$link = PrepareLink($_POST["url"]);
	$response = LoadData("http://api.soundcloud.com/resolve.json?url={$link}&client_id={$api}");
	if (IsNullOrEmpty($response)) {
		//Logger(LogType::ERROR, ErrorCode::NO_RESPONSE, $link);
		include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-unavailable.php');
		exit;
	}
	$data = json_decode($response["content"]);
	if (IsNullOrEmpty($data)) {
		//Logger(LogType::ERROR, ErrorCode::NO_JSON, $link);
		include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-unavailable.php');
		exit;
	}
	if (array_key_exists('errors', $data)) {
		header('refresh:5;url=../');
		//Logger(LogType::ERROR, ErrorCode::ANOTHER_SC_LINK, $link);
		echo GetTemplate($_SERVER['DOCUMENT_ROOT'].'/templates/template-invalid-soundcloud.php', array('url' => $_POST['url']));
		exit;
	}
	if (!array_key_exists('kind', $data) || IsNullOrEmpty($data->{'kind'})) {
		header('refresh:5;url=../');
		//Logger(LogType::ERROR, ErrorCode::NO_KIND_KEY, $link);
		echo GetTemplate($_SERVER['DOCUMENT_ROOT'].'/templates/template-invalid-soundcloud.php', array('url' => $_POST['url']));
		exit;
	}

	// #################################################################################################################
	// TRACK
	if ($data->{'kind'} === "track") {
		$id = $data->{'id'};
		if (IsNullOrEmpty($id)) {
			//Logger(LogType::ERROR, ErrorCode::NO_TRACK_ID, $link);
			include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-unavailable.php');
			exit;
		}
		$title = $data->{'title'};
		$thumbnail = str_replace(array("mini", "tiny", "small", "badge", "t67x67", "large", "t300x300", "crop"), "crop", $data->{'artwork_url'});
		if (IsNullOrEmpty($thumbnail) || strlen($thumbnail) == 0)
			$thumbnail = str_replace(array("mini", "tiny", "small", "badge", "t67x67", "large", "t300x300", "crop"), "crop", $data->{'user'}->{'avatar_url'});
		$thumbnail_block = (IsNullOrEmpty($thumbnail) || strlen($thumbnail) == 0 || strpos($thumbnail, 'default_avatar') !== false) ? '<div class="cover"></div>' : '<img src="'.$thumbnail.'" alt="'.$title.'">';
		$stream_url = GetStreamUrl($id);
		$headers = get_headers($stream_url, 1);
		if (!array_key_exists('Location', $headers)) {
			SendReport("There is no \"Location\" key in headers.");
			//Logger(LogType::ERROR, ResultCode::TRACK, $_POST['url']);
			header('Context: 0');
			header('Location: ../oops/');
			exit;
		}
		$stream_url_real = $headers["Location"];
		$args = array(
			'url'               => $link,
			'thumbnail_block'   => $thumbnail_block,
			'title'             => $title,
			'user'              => $data->{'user'}->{'username'},
			'file_size'         => FormatBytes(GetBytes($headers)),
			'stream_url'		=> $stream_url,
			'stream_url_real'   => $stream_url_real,
			'file_name'         => str_replace(' ', '&nbsp;', $title)
		);
		//Logger(LogType::SUCCESS, ResultCode::TRACK, $link);
		echo GetTemplate($_SERVER['DOCUMENT_ROOT'].'/templates/template-track.php', $args);
		exit;
	}

	// #################################################################################################################
	// PLAYLIST
	else if ($data->{'kind'} === "playlist") {
		$title = $data->{'title'};
		$tracks = $data->{'tracks'};
		if (is_null($tracks)) {
			//Logger(LogType::ERROR, ErrorCode::NO_TRACKS, $link);
			include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-unavailable.php');
			exit;
		}
		$duration = $data->{'duration'};
		$thumbnail = str_replace(array("mini", "tiny", "small", "badge", "t67x67", "large", "t300x300", "crop"), "crop", $data->{'artwork_url'});
		if (IsNullOrEmpty($thumbnail) || strlen($thumbnail) == 0)
			$thumbnail = str_replace(array("mini", "tiny", "small", "badge", "t67x67", "large", "t300x300", "crop"), "crop", $data->{'tracks'}[0]->{'artwork_url'});
		$thumbnail_block = (IsNullOrEmpty($thumbnail) || strlen($thumbnail) == 0 || strpos($thumbnail, 'default_avatar') !== false) ? '<div class="cover"></div>' : '<img src="'.$thumbnail.'" alt="'.$title.'">';
		$args = array(
			'tracks'            => $tracks,
			'thumbnail_block'   => $thumbnail_block,
			'title'             => $title,
			'user'              => $data->{'user'}->{'username'},
			'track_count'       => $data->{'track_count'},
			'duration'          => formatMilliseconds($duration),
			'likes'             => $data->{'likes_count'}
		);
		//Logger(LogType::SUCCESS, ResultCode::PLAYLIST, $link);
		echo GetTemplate($_SERVER['DOCUMENT_ROOT'].'/templates/template-playlist.php', $args);
		exit;
	}

	// #################################################################################################################
	// USER
	else if ($data->{'kind'} === "user") {
		$user_id = $data->{'id'};
		if (IsNullOrEmpty($user_id)) {
			//Logger(LogType::ERROR, ErrorCode::NO_USER_ID, $link);
			include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-unavailable.php');
			exit;
		}
		$user = $data->{'username'};
		$location_block = '';
		$location = $data->{'country'};
		$location = IsNullOrEmpty($location) ? $data->{'city'} : $location.', '.$data->{'city'};
		if (!IsNullOrEmpty($location))
			$location_block = '<span class="icon icon-location hidden-mobile">'.$location.'</span>';
		$thumbnail = str_replace(array("mini", "tiny", "small", "badge", "t67x67", "large", "t300x300", "crop"), "crop", $data->{'avatar_url'});
		$thumbnail_block = (IsNullOrEmpty($thumbnail) || strlen($thumbnail) == 0 || strpos($thumbnail, 'default_avatar') !== false) ? '<div class="cover"></div>' : '<img src="'.$thumbnail.'" alt="'.$user.'">';
		$response = LoadData("https://api.soundcloud.com/users/{$user_id}/tracks.json?client_id={$api}");
		if (IsNullOrEmpty($response)) {
			Logger(LogType::ERROR, ErrorCode::NO_RESPONSE, $link);
			include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-unavailable.php');
			exit;
		}
		$tracks = json_decode($response["content"]);
		if (is_null($tracks)) {
			Logger(LogType::ERROR, ErrorCode::NO_TRACKS, $link);
			include_once($_SERVER['DOCUMENT_ROOT'].'/templates/template-unavailable.php');
			exit;
		}
		$args = array(
			'tracks'            => $tracks,
			'thumbnail_block'   => $thumbnail_block,
			'user'              => $user,
			'full_name'         => $data->{'full_name'},
			'location_block'    => $location_block,
			'track_count'       => $data->{'track_count'},
			'playlist_count'    => $data->{'playlist_count'}
		);
		//Logger(LogType::SUCCESS, ResultCode::USER, $link);
		echo GetTemplate($_SERVER['DOCUMENT_ROOT'].'/templates/template-user.php', $args);
		exit;
	}

	// #################################################################################################################
	// ERROR
	else {
		header('refresh:15;url=../');
		//Logger(LogType::ERROR, ErrorCode::ANOTHER_SC_LINK, $link);
		echo GetTemplate($_SERVER['DOCUMENT_ROOT'].'/templates/template-invalid-soundcloud.php', array('url' => $_POST['url']));
		exit;
	}
}
// #####################################################################################################################
// ERROR
else {
	header('refresh:15;url=../');
	//Logger(LogType::ERROR, ErrorCode::INVALID_LINK, $_POST['url']);
	echo GetTemplate($_SERVER['DOCUMENT_ROOT'].'/templates/template-invalid-link.php', array('url' => $_POST['url']));
	exit;
}
?>
