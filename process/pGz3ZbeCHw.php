<?php
require 'global.php';
if (isset($_POST['id']) && isset($_POST['title'])) {
	$headers = get_headers("https://api.soundcloud.com/tracks/{$_POST['id']}/stream?client_id={$api}", 1);
	if (!array_key_exists('Location', $headers)) {
		header('Location: ../');
		exit;
	}
	$url = $headers["Location"];

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_NOBODY, 0);
	curl_setopt($ch, CURLOPT_TIMEOUT, 5);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	$output = curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	if ($status == 200) {
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=".$_POST['title'].".mp3");
		echo $output;
		exit;
	}
	else{
		echo "";
	}
}

else{
	header('Location: ../');
	exit;
}
?>
