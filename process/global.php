<?php
$api = 'NmW1FlPaiL94ueEu7oziOWjYEzZzQDcK';

function IsNullOrEmpty($value) {
	return ($value === NULL || $value == NULL);
}






// REMOVE THIS
function ValidateLink($url) {
	return (strpos($url, 'soundcloud.com/') !== false);
}






function GetTemplate($file, $args) {
	if (!file_exists($file))
		return '';
	if (is_array($args))
		extract($args);
	ob_start();
	include $file;
	return ob_get_clean();
}

function IsUrl($url) {
	$path = parse_url($url, PHP_URL_PATH);
	$encoded_path = array_map('urlencode', explode('/', $path));
	$url = str_replace($path, implode('/', $encoded_path), $url);
	return filter_var($url, FILTER_VALIDATE_URL) ? true : false;
}

function IsUrlSoundcloud($url) {
	return (strpos($url, 'soundcloud.com/') !== false);
}

function PrepareLink($url) {
	return "https://".strstr($url, 'soundcloud.com/');
}

function LoadData($url) {
	$options = array(
		CURLOPT_RETURNTRANSFER => true,     // return web page
		CURLOPT_HEADER         => false,    // don't return headers
		CURLOPT_FOLLOWLOCATION => true,     // follow redirects
		CURLOPT_ENCODING       => "",       // handle all encodings
		CURLOPT_USERAGENT      => "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0", // who am i
		CURLOPT_AUTOREFERER    => true,     // set referer on redirect
		CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
		CURLOPT_TIMEOUT        => 120,      // timeout on response
		CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
		CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
	);
	$ch      = curl_init($url);
	curl_setopt_array($ch, $options);
	$content = curl_exec($ch);
	$err     = curl_errno($ch);
	$errmsg  = curl_error($ch);
	$header  = curl_getinfo($ch);
	curl_close($ch);

	$header['errno']   = $err;
	$header['errmsg']  = $errmsg;
	$header['content'] = $content;
	return $header;
}

function GetStreamUrl($id) {
	global $api;
	return "https://api.soundcloud.com/tracks/{$id}/stream?client_id={$api}";
}

function GetBytes($headers) {
	return array_key_exists('Content-Length', $headers) ? (int)$headers["Content-Length"][1] : "unknown";
}



function FormatBytes($size, $decimals = 2){
	$unit = array(
		'0' => 'Byte',
		'1' => 'KB',
		'2' => 'MB',
		'3' => 'GB',
		'4' => 'TB',
		'5' => 'PB',
		'6' => 'EB',
		'7' => 'ZB',
		'8' => 'YB'
	);
	for($i = 0; $size >= 1000 && $i <= count($unit); $i++){
		$size = $size/1000;
	}
	return round($size, $decimals).' '.$unit[$i];
}



function formatMilliseconds($milliseconds) {
	$seconds = floor($milliseconds / 1000);
	$minutes = floor($seconds / 60);
	$hours = floor($minutes / 60);
	$milliseconds = $milliseconds % 1000;
	$seconds = $seconds % 60;
	$minutes = $minutes % 60;

	if($hours > 0)
		$time = sprintf('%u hours %02u min %02u sec', $hours, $minutes, $seconds);
	else if($minutes > 0)
		$time = sprintf('%02u min %02u sec', $minutes, $seconds);
	else if($seconds > 0)
		$time = sprintf('%02u sec', $seconds);
	return rtrim($time, '0');
}



function SendReport($message){
	$mail_headers = "Content-type: text/html; charset=utf-8 \r\n";
	$mail_headers .= "From: SoundCloud Downloader <support@soundcloudmp3downloader.com>\r\n";
	$mail = mail("pro100dreamteam@gmail.com", "Bug report", $message, $mail_headers);
	return $mail ? true : false;
}



class LogType {
	const SUCCESS   = 1;
	const ERROR     = 2;
	const SHARE     = 3;
	const DOWNLOAD  = 4;
}

class ResultCode {
	const TRACK     = 1;
	const PLAYLIST  = 2;
	const USER      = 3;
	const SEARCH    = 4;
}

class ErrorCode {
	const INVALID_LINK      = 1;
	const NO_RESPONSE       = 2;
	const NO_JSON           = 3;
	const NO_KIND_KEY       = 4;
	const NO_TRACK_ID       = 5;
	const NO_TRACKS         = 6;
	const NO_USER_ID        = 7;
	const ANOTHER_SC_LINK   = 8;
}

function Logger($logType, $codeId, $query){
	if (IsNullOrEmpty($logType) || IsNullOrEmpty($codeId) || IsNullOrEmpty($query))
		return;
	$address = "127.0.0.1";
	$username = "soundcloudmp_usr";
	$password = "phSAOGx2F9SAVoMs";
	$database = "soundcloudmp3_db";
	try {
		$connector = new mysqli($address, $username, $password, $database);
		if ($connector->connect_error) {
			SendReport("Connection failed: " . $connector->connect_error);
			die();
		}

		$table = 'LogSuccess';
		$codeName = 'RequestId';

		if ($logType == LogType::ERROR)
		{
			$table = 'LogError';
			$codeName = 'ErrorId';
		}
		else if ($logType == LogType::SHARE)
			$table = 'LogShare';
		else if ($logType == LogType::DOWNLOAD)
			$table = 'LogDownload';

		$ip = inet_pton($_SERVER['REMOTE_ADDR']);

		$sql = "INSERT INTO $table ($codeName, `Query`, Ip) VALUES ('$codeId', '$query', '$ip')";
		if ($connector->query($sql) === FALSE)
			echo "Error: " . $sql . "<br>" . $connector->error;
		$connector->close();
	}
	catch (Exception $e) {
		SendReport("Logger exception: " . $e->getMessage());
	}
	return;
}

?>
